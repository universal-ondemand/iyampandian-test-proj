#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      ueindia
#
# Created:     07/04/2020
# Copyright:   (c) ueindia 2020
# Licence:     <your licence>
#-------------------------------------------------------------------------------

# importing the requests library
import requests

datas = {"name": "Trigger using python", "description": "trigger Build", "tags": "tag_py", "scenarios": ["TestScenario.py"]}
url = "http://172.20.10.2:8080/api/run/start"
headers = {"Content-Type": "application/json" , "Accept": "application/json"}

rsp = requests.post(url, json=datas, headers=headers)

print (rsp.text)
print(rsp.json())
print(rsp.status_code)
